const User = require('../models/User');

const controller = {}

controller.addFavGet = async (req, res, next) => {   
    
    try{
        const users = await User.findById(req.user._id).populate("pets");
        const {pets} = users;
        return res.status(200).json(pets);

    }catch(error) {
        return res.status(500).json(error);
    }
}

controller.addFavPost = async (req, res, next) => {
    
    try {
        const { petId } = req.body;
        const userId = req.user._id;

        const addFavs = await User.findByIdAndUpdate(
            userId,
            { $addToSet: { pets: petId } },  
            { new: true }
        );

        return res.status(200).json(addFavs);                                                        
            
    } catch (error) {
        return res.status(500).json(error);
        
    }
}

module.exports = controller;