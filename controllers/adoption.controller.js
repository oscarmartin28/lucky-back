const Adoption = require('../models/Adoption.js');

const controller = {}

controller.adoptionPost = async (req, res, next) => {
    try {
        const {
            name,
            email,
            phone,
            dni,
            address,
            postalCode,   
            city,      
        } = req.body;

        const newAdoption = new Adoption ({
          name,
          email,
          phone,
          dni,
          address,
          postalCode,
          city,
        });
        const createdAdoption = await newAdoption.save();
        return res.status(201).json(createdAdoption);

    } catch (error) {
         return res.status(500).json(error);
      }
}

module.exports = controller;