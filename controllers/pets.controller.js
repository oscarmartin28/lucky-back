const Pet = require('../models/Pet.js');

const petGet = async (req, res) => {
    try {
        const pets = await Pet.find();
        return res.status(200).json(pets);
    } catch (err) {
        return res.status(500).json(err);
    }
};

const petFilter = async (req, res) => {
  try {
      const filterPets = await Pet.find();
      return res.status(200).json(filterPets);
  } catch (err) {
      return res.status(500).json(err);
  }
};

const idGet = async (req, res) => {
    const id = req.params.id;
  
    try {
      const petFinded = await Pet.findById(id);
      return res.status(200).json(petFinded);
  
    } catch (error) {
      return res.status(500).json(error);
    }
};

const createPost = async (req, res, next) => {
    console.log(req.file);

    // ESTO CON CLAUDINARY  =>  const mascotaPicture = req.file_url;
    
    const petPicture = req.file ? req.file.filename : null;
  
      try {
        // Crearemos una instancia de mascota con los datos enviados
        const newPet = new Pet ({
          name: req.body.name,
          city: req.body.city,
          info: req.body.info,
          health: req.body.health,
          adoption: req.body.adopcion,
          picture: petPicture,
          pedigree: req.body.pedigree,
          size: req.body.size,
        });
    
        // Guardamos la mascota en la DB
        await newPet.save();
        return res.redirect('/pets');
      } catch (err) {
            // Lanzamos la función next con el error para que gestione todo Express
        next(err);
      }
};

const editPut = async (req, res, next) => {
    try {
      const id = req.body.id;
  
      const updatedPet = await Pet.findByIdAndUpdate(
        id, // La id para encontrar el documento a actualizar
        { 
            name: req.body.name,
            city: req.body.city,
            info: req.body.info,
            health: req.body.health,
            adoption: req.body.adoption,
            picture: petPicture,
            pedigree: req.body.pedigree,
            size: req.body.size,
        },
         // Campos que actualizaremos
        { new: true } // Usando esta opción, conseguiremos el documento actualizado cuando se complete el update
      );
  
      return res.status(200).json(updatedPet);
    } catch (err) {
      next(err);
    }
};

const petDelete = async (req, res, next) => {
    const { id } = req.params;
  
    try {
      await Pet.findByIdAndDelete(id);
  
      res.status(200).json("Pet deleted");
    } catch (error) {
      next(error);
    }
};

module.exports = {
    petGet,
    idGet,
    createPost,
    editPut,
    petDelete,
    petFilter,
}