const mongoose = require('mongoose');
const DB_URL = require('../db.js').DB_URL;
const Pet = require('../models/Pet');

const pets = [
  {
    name: "Molly",
    age: "Adulto",
    city: "Madrid",
    info: {
        species: 'Perro',
        birthday: '23-12-2016',
        gender: 'Macho',
        size: "Pequeño",
        weight: 24,
        personality: 'Cariñoso',
        story: 'Fue rescatado por una familia',
    },
    health: {
        vaccinated: true,
        dewormed: true,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 100,
        shippingCity: 'Madrid',
    },
    picture: "/img/pets/perro-Molly.jpg",
    pedigree: true,
    icon: "/img/pets_icon/green-dog.png",
  },
  {
    name: "Isco",
    age: "Joven",
    city: "Toledo",
    info: {
        species: 'Perro',
        birthday: '21-07-2016',
        gender: 'Macho',
        size: "Grande",
        weight: 24,
        personality: 'Jugetón y divertido',
        story: 'Fue abandonado desde muy pequeño',
    },
    health: {
        vaccinated: true,
        dewormed: false,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 70,
        shippingCity: 'Toledo',
    },
    picture: "/img/pets/perro-Isco.jpg",
    pedigree: true,
    icon: "/img/pets_icon/green-dog.png",
  },
  {
    name: "Benito",
    age: "Bebe",
    city: "Sevilla",
    info: {
        species: 'Gato',
        birthday: '27-12-2011',
        gender: 'Macho',
        size: "Mediano",
        weight: 13,
        personality: 'Timido',
        story: 'Gato callejero desde muy temprana edad',
    },
    health: {
        vaccinated: true,
        dewormed: true,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 120,
        shippingCity: 'Sevilla',
    },
    picture: "/img/pets/gato-Benito.jpg",
    pedigree: true,
    icon: "/img/pets_icon/cat.png",
  },
  {
    name: "Blanca",
    age: "Adulto",
    city: "Madrid",
    info: {
        species: 'Gato',
        birthday: '15-05-2018',
        gender: 'Hembra',
        size: "Mediano",
        weight: 12,
        personality: 'Inteligente',
        story: 'Se escapó de su anterior familia',
    },
    health: {
        vaccinated: true,
        dewormed: false,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 60,
        shippingCity: 'Madrid',
    },
    picture: "/img/pets/gato-Blanca.jpg",
    pedigree: true,
    icon: "/img/pets_icon/cat.png",

  },
  {
    name: "Ruby",
    age: "Adulto",
    city: "Madrid",
    info: {
        species: 'Conejo',
        birthday: '12-12-2017',
        gender: 'Hembra',
        size: "Pequeño",
        weight: 4,
        personality: 'Activo',
        story: 'Encontrado en la calle',
    },
    health: {
        vaccinated: true,
        dewormed: true,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 40,
        shippingCity: 'Madrid',
    },
    picture: "/img/pets/conejo-Ruby.jpg",
    pedigree: true,
    icon: "/img/pets_icon/rabbit.png",
  },
  {
    name: "Lisa",
    age: "Joven",
    city: "Cuenca",
    info: {
        species: 'Conejo',
        birthday: '23-12-2020',
        gender: 'Hembra',
        size: "Pequeño",
        weight: 5,
        personality: 'Arisco',
        story: 'Encontrado en el campo',
    },
    health: {
        vaccinated: true,
        dewormed: true,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 50,
        shippingCity: 'Valencia',
    },
    picture: "/img/pets/conejo-Lisa.jpg",
    pedigree: true,
    icon: "/img/pets_icon/rabbit.png",
  },
  {
    name: "Tobias",
    age: "Adulto",
    city: "Canarias",
    info: {
        species: 'Cobaya',
        birthday: '23-12-2019',
        gender: 'Macho',
        size: "Pequeño",
        weight: 2,
        personality: 'Mimoso',
        story: 'Fue rescatado por una familia',
    },
    health: {
        vaccinated: true,
        dewormed: false,
        healthy: false,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 30,
        shippingCity: 'Canarias',
    },
    picture: "/img/pets/cobaya-Tobias.jpg",
    pedigree: true,
    icon: "/img/pets_icon/guineapig.png",
  },
  {
    name: "Lucho",
    age: "Bebe",
    city: "Almería",
    info: {
        species: 'Cobaya',
        birthday: '07-12-1018',
        gender: 'Macho',
        size: "Pequeño",
        weight: 1,
        personality: 'Divertido y alborotador',
        story: 'Fue rescatado por una familia',
    },
    health: {
        vaccinated: true,
        dewormed: true,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 35,
        shippingCity: 'Almería',
    },
    picture: "/img/pets/cobaya-Lucho.jpg",
    pedigree: true,
    icon: "/img/pets_icon/guineapig.png",
  },
  {
    name: "Juanita",
    age: "Bebe",
    city: "Toledo",
    info: {
        species: 'Pequeño mamifero (chinchilla)',
        birthday: '30-03-2020',
        gender: 'Hembra',
        size: "Pequeño",
        weight: 5,
        personality: 'Divertido',
        story: 'Fue rescatado por una familia',
    },
    health: {
        vaccinated: false,
        dewormed: true,
        healthy: true,
        identified: false,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 25,
        shippingCity: 'Toledo',
    },
    picture: "/img/pets/chinchilla-Juanita.jpg",
    pedigree: true,
    icon: "/img/pets_icon/mammal.png",
  },
  {
    name: "Misi",
    age: "Adulto",
    city: "Madrid",
    info: {
        species: 'Pequeño mamifero (ratón)',
        birthday: '03-01-2021',
        gender: 'Macho',
        size: "Pequeño",
        weight: 11,
        personality: 'Inteligente y juguetón',
        story: 'Fue rescatado por una familia',
    },
    health: {
        vaccinated: true,
        dewormed: true,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 30,
        shippingCity: 'Madrid',
    },
    picture: "/img/pets/raton-Misi.jpg",
    pedigree: true,
    icon: "/img/pets_icon/mammal.png",
  },
  {
    name: "Pedro",
    age: "Joven",
    city: "Cantabria",
    info: {
        species: 'Huron',
        birthday: '23-12-2017',
        gender: 'Macho',
        size: "Pequeño",
        weight: 2,
        personality: 'Tranquilo',
        story: 'Fue rescatado por una familia',
    },
    health: {
        vaccinated: true,
        dewormed: false,
        healthy: false,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 20,
        shippingCity: 'Cantabria',
    },
    picture: "/img/pets/huron-Pedro.jpg",
    pedigree: true,
    icon: "/img/pets_icon/huron.png",
  },
  {
    name: "Moritz",
    age: "Anciano",
    city: "Madrid",
    info: {
        species: 'Huron',
        birthday: '11-12-2020',
        gender: 'Macho',
        size: "Pequeño",
        weight: 3,
        personality: 'Mimoso y divertido',
        story: 'Fue rescatado por una familia',
    },
    health: {
        vaccinated: true,
        dewormed: true,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 20,
        shippingCity: 'Madrid',
    },
    picture: "/img/pets/huron-Moritz.jpg",
    pedigree: true,
    icon: "/img/pets_icon/huron.png",
  },
  {
    name: "Lula",
    age: "Adulto",
    city: "Murcia",
    info: {
        species: 'Pez',
        birthday: '23-12-2020',
        gender: 'Macho',
        size: "Pequeño",
        weight: 0.1,
        personality: 'Nervioso',
        story: 'Tienda de aniMachos',
    },
    health: {
        vaccinated: true,
        dewormed: false,
        healthy: true,
        identified: false,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 10,
        shippingCity: 'Murcia',
    },
    picture: "/img/pets/pez-Lula.jpg",
    pedigree: true,
    icon: "/img/pets_icon/fish.png",
  },
  {
    name: "Lupita",
    age: "Joven",
    city: "Pontevedra",
    info: {
        species: 'Pez',
        birthday: '11-12-2020',
        gender: 'Hembra',
        size: "Pequeño",
        weight: 0.1,
        personality: 'Independiente',
        story: 'Encontrado en un rio',
    },
    health: {
        vaccinated: false,
        dewormed: false,
        healthy: true,
        identified: false,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 10,
        shippingCity: 'Pontevedra',
    },
    picture: "/img/pets/pez-Lupita.jpg",
    pedigree: true,
    icon: "/img/pets_icon/fish.png",
  },
  {
    name: "Nagini",
    age: "Anciano",
    city: "Madrid",
    info: {
        species: 'Reptil (Pitón)',
        birthday: '23-12-1958',
        gender: 'Hembra',
        size: "Grande",
        weight: 50,
        personality: 'Agresiva',
        story: 'La serpiente de Voldemort',
    },
    health: {
        vaccinated: true,
        dewormed: true,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 40,
        shippingCity: 'Madrid',
    },
    picture: "/img/pets/reptil-Nagini.jpg",
    pedigree: true,
    icon: "/img/pets_icon/reptile.png",
  },
  {
    name: "Pepito",
    age: "Anciano",
    city: "Teruel",
    info: {
        species: 'Reptil (Iguana)',
        birthday: '28-12-2020',
        gender: 'Macho',
        size: "Pequeño",
        weight: 1,
        personality: '',
        story: 'Encontrado en la selva',
    },
    health: {
        vaccinated: true,
        dewormed: true,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 60,
        shippingCity: 'Teruel',
    },
    picture: "/img/pets/reptil-Pepito.jpg",
    pedigree: true,
    icon: "/img/pets_icon/reptile.png",
  },
  {
    name: "Lucia",
    age: "Bebe",
    city: "Madrid",
    info: {
        species: 'Anfibio (rana)',
        birthday: '23-12-2019',
        gender: 'Hembra',
        size: "Pequeño",
        weight: 0.8,
        personality: 'Venenosa',
        story: 'Fue rescatado por una familia',
    },
    health: {
        vaccinated: true,
        dewormed: true,
        healthy: false,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 15,
        shippingCity: 'Madrid',
    },
    picture: "/img/pets/rana-Lucia.jpg",
    pedigree: true,
    icon: "/img/pets_icon/amphibian.png",
  },
  {
    name: "Luna",
    age: "Joven",
    city: "Zaragoza",
    info: {
        species: 'Anfibio (rana)',
        birthday: '23-12-1016',
        gender: 'Hembra',
        size: "Pequeño",
        weight: 4,
        personality: '',
        story: 'Fue rescatado de una charca',
    },
    health: {
        vaccinated: true,
        dewormed: false,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 10,
        shippingCity: 'Zaragoza',
    },
    picture: "/img/pets/rana-Luna.jpg",
    pedigree: true,
    icon: "/img/pets_icon/amphibian.png",
  },
  {
    name: "Casper",
    age: "Joven",
    city: "Barcelona",
    info: {
        species: 'Aracnido o Insecto',
        birthday: '23-01-2021',
        gender: 'Hembra',
        size: "Pequeño",
        weight: 0.2,
        personality: '',
        story: 'Fue rescatado por una familia',
    },
    health: {
        vaccinated: true,
        dewormed: true,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 20,
        shippingCity: 'Barcelona',
    },
    picture: "/img/pets/mariposa-Casper.jpg",
    pedigree: true,
    icon: "/img/pets_icon/spider.png",
  },
  {
    name: "Peter",
    age: "Anciano",
    city: "Cádiz",
    info: {
        species: 'Aracnido o Insecto',
        birthday: '23-12-2020',
        gender: 'Macho',
        size: "Pequeño",
        weight: 0.1,
        personality: 'Saltarina',
        story: 'Fue rescatado por una familia',
    },
    health: {
        vaccinated: true,
        dewormed: false,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 10,
        shippingCity: 'Cádiz',
    },
    picture: "/img/pets/araña-Peter.jpg",
    pedigree: true,
    icon: "/img/pets_icon/spider.png",
  },
  {
    name: "Chispas",
    age: "Adulto",
    city: "Madrid",
    info: {
        species: 'Ave (Canario)',
        birthday: '23-12-2015',
        gender: 'Hembra',
        size: "Pequeño",
        weight: 6,
        personality: 'Cantarín y malo posando',
        story: 'Fue rescatado por una familia',
    },
    health: {
        vaccinated: true,
        dewormed: true,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 25,
        shippingCity: 'Madrid',
    },
    picture: "/img/pets/canario-Chispas.jpg",
    pedigree: true,
    icon: "/img/pets_icon/green-bird.png",
  },
  {
    name: "Pipi",
    age: "Joven",
    city: "Madrid",
    info: {
        species: 'Ave',
        birthday: '23-04-2018',
        gender: 'Macho',
        size: "Pequeño",
        weight: 5,
        personality: 'Muy cotilla, lo repite todo',
        story: 'Fue rescatado por una familia',
    },
    health: {
        vaccinated: true,
        dewormed: true,
        healthy: true,
        identified: true,
        microchip: false,
        toKnow: '',
    },
    adoption: {
        requirements: ['Ninguno'],
        rate: 15,
        shippingCity: 'Madrid',
    },
    picture: "/img/pets/loro-Pipi.jpg",
    pedigree: true,
    icon: "/img/pets_icon/green-bird.png",
  },
];

mongoose.connect(DB_URL, {
   useNewUrlParser: true,
   useUnifiedTopology: true, 
})
.then(async () => {
    const allPets = await Pet.find();

    if(allPets.length) {
        await Pet.collection.drop();
    }
})
.catch((err) => {
    console.log(`Error deleting db data ${err}`);
})
.then(async () => {
    await Pet.insertMany(pets);
})
.catch((err) => {
    console.log(`Error adding data to our db ${err}`)
})
.finally(() => mongoose.disconnect());