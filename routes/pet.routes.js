const express = require('express');
const petsController = require('../controllers/pets.controller');
const fileMiddlewares = require('../middlewares/file.middleware');
const router = express.Router();

router.get('/', petsController.petGet);
router.get('/:id', petsController.idGet);


module.exports = router;