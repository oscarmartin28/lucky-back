const express = require('express');
const favsController = require('../controllers/favs.controller');

const router = express.Router();

router.get('/', favsController.addFavGet);
router.post('/', favsController.addFavPost);

module.exports = router;