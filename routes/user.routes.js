const express = require('express');
const fileMiddlewares = require('../middlewares/file.middleware');
const usersController = require('../controllers/users.controller');

const router = express.Router();

router.post('/register', [fileMiddlewares.upload.single('picture'), fileMiddlewares.uploadToCloudinary], usersController.registerPost);
router.post('/login', usersController.loginPost);
router.post('/logout', usersController.logoutPost);
router.get('/check', usersController.checkSession);

module.exports = router;