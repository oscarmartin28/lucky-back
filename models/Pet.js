const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const petSchema = new Schema(
  {
    name: { type: String, required: true },
    age: [{ type: String, enum: ["Bebe", "Joven", "Adulto", "Anciano"] }],
    city: { type: String, required: true },
    info: { 
        species: { type: String },
        birthday: { type:  String},
        gender: [{ type: String, enum: ["Macho", "Hembra"] }],
        size: [{ type: String, enum: ["Pequeño", "Mediano", "Grande"] }],
        weight: { type: Number },
        personality: { type: String },
        story: { type: String },
     },
     health: { 
        vaccinated: { type: Boolean },
        dewormed: { type: Boolean },
        healthy: { type: Boolean },
        identified: { type: Boolean },
        microchip: { type: Boolean },
        toKnow: { type: String },
     },
     adoption: { 
        requirements: { type: [String] },
        rate: { type: Number },
        shippingCity: { type: String },
     },
    picture: { type: String },
    pedigree: { type: Boolean },
    icon: { type: String },
  },
  {
    timestamps: true,
  }
);

const Pet = mongoose.model("Pets", petSchema);

module.exports = Pet;