const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    email: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String, required: true },
    picture: { type: String },
    pets: [{ type: mongoose.Types.ObjectId, ref: 'Pets' }],
    role: {
      enum: ["user", "shelter-owner"],
      type: String,
      default: "user",
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const User = mongoose.model("Users", userSchema);

module.exports = User;
