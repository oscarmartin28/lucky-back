const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const adoptionSchema = new Schema(
  {
    name: { type: String },
    email: { type: String },
    phone: { type: String },
    dni: { type: String },
    address: { type: String },
    postalCode: { type: String },
    city: { type: String },
  },
  {
    timestamps: true,
  }
);

const Adoption = mongoose.model("Adoptions", adoptionSchema);

module.exports = Adoption;