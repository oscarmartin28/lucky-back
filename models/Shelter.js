const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const shelterSchema = new Schema(
  {
    email: { type: String, required: true },
    name: { type: String, required: true },
    password: { type: String, required: true },
    picture: { type: String },
    pets: [{ type: mongoose.Types.ObjectId, ref: 'Pets' }],
    role: {
      enum: ["admin", "basic", "shelter-owner"],
      type: String,
      default: "basic",
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const Shelter = mongoose.model("Shelters", shelterSchema);

module.exports = Shelter;
