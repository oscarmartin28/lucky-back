const express = require('express');
const path = require('path');
const passport = require('passport');
const cors = require('cors');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

const userRouter = require('./routes/user.routes');
const petRouter = require('./routes/pet.routes');
const favsRouter = require('./routes/favs.routes');
const adoptionRouter = require('./routes/adoption.routes');

require('dotenv').config();

const db = require('./db.js');
const { NONAME } = require('dns');
db.connect();

require('./passport');

const PORT = process.env.PORT || 4000;
const server = express();

server.use((req, res, next) => {
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

server.use(cors({
  origin: ['http://localhost:3000', /* "url de netlify" */],
  credentials: true,
}));

server.use(express.json());
server.use(express.urlencoded({ extended: true }));

server.use(express.static(path.join(__dirname, 'public')));
/* server.set('trust proxy', true); */

server.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 999999999999,
      sameSite: "none",
      secure: true,
    },
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
  })
);

server.use(passport.initialize());
server.use(passport.session());

//Rutas
server.use("/user", userRouter);
server.use('/pets', petRouter);
server.use('/favs', favsRouter);
server.use('/adoption', adoptionRouter);


server.use('*', (req, res, next) => {
  const error = new Error('Route not found');
  error.status = 404;
  next(error);
})

server.use((err, req, res, next) => {
  return res.status(err.status || 500).json(err);
});

server.listen(PORT, () => {
  console.log(`Server started on http://localhost:${PORT}`);
});